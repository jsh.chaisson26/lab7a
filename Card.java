public class Card
{
	
	private String suit;
	private String value;
	
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit(){
		return this.suit;
	}
	
	public String getValue(){
		return this.value;
	}
	
	public String toString(){
		return "The suit of the card is " + suit + " and the value is " + value;
	}
	
	public double calculateScore(){
		String[] suits = new String[] {"clubs", "diamonds", "spades", "hearts"};
		String[] values = new String[] {"ace", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "jack", "queen", "king"};
		double beforeDecimal = 0.0;
		double afterDecimal = 0.0;
		for(int i=0; i < values.length; i++){
			if(this.value.equals(values[i])){
				beforeDecimal = i+1;
			}
		}
		double counter = 0;
		for(int i=0; i < suits.length; i++){
			if(this.suit.equals(suits[i])){
				afterDecimal = (counter+1)/10;
			}
			counter++;
		}
		return beforeDecimal + afterDecimal;
	}
}