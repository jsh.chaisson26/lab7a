public class SimpleWar
{
	public static void main(String[] args){
		Deck myDeck = new Deck();
		myDeck.shuffle();
		
		int playerOnePoints = 0;
		int playerTwoPoints = 0;
		
		for(int i =0;myDeck.length() != 0; i++){
			Card firstCard = myDeck.drawTopCard();
			double firstScore = firstCard.calculateScore();
			Card secondCard = myDeck.drawTopCard();
			double secondScore = secondCard.calculateScore();
			System.out.println("ROUND: " + (i+1));
			System.out.println("PLAYER ONE: " + playerOnePoints);
			System.out.println("PLAYER TWO: " + playerTwoPoints);
			System.out.println(firstCard);
			System.out.println("The score of this first card is " + firstScore);
			System.out.println(secondCard);
			System.out.println("The score of this second card is " + secondScore);
			if(firstScore > secondScore){
				playerOnePoints +=1;
				System.out.println("Player one has won this round");
			}
			else{
				playerTwoPoints +=1;
				System.out.println("Player two has won this round");
			}
			System.out.println("player one has " + playerOnePoints + " points and player two has " + playerTwoPoints + " points");
		}
	}
}